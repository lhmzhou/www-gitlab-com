/* global jQuery */
(function($) {
  'use strict';

  var filterValues = {};

  $('#courses-filters > select').on('change', function() {
    filterValues[this.name] = this.value;
    applyFilters();
  });

  function applyFilters() {
    var $courses = $('.resource');
    var dataSelector = buildSelector();

    if (dataSelector !== '') {
      $courses.addClass('hidden');
      $courses.filter(dataSelector).removeClass('hidden');
    } else {
      $courses.removeClass('hidden');
    }
  }

  function buildSelector() {
    return Object.keys(filterValues)
      .filter(function (key) {
        return !!filterValues[key];
      })
      .map(function (key) {
        return `[data-${key}="${filterValues[key]}"]`
      })
      .join('');
  }
}(jQuery));
