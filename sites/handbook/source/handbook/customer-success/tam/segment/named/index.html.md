---
layout: handbook-page-toc
title: "TAM Segment: Named"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

⚠️ This information is under active development and is not yet final. This page will be updated on an ongoing basis during this phase.
{: .alert .alert-warning}

## Overview

Definition: Named TAM on the account, product usage data-based success goals per account, programmatic enablement.

## Motions

### Align

Here Success Plans are more focused on quantitative objectives such as use case adoption, tied into value drivers as determined in the pre-sales process, to ensure platform value is realized. Progress and success against these objectives are reported upon in the EBRs that will be held with those customers that have product usage data available.

#### Metrics for Align

1. 100% net-new customers with success plans (value drivers, use cases)
1. EBRs held with 50% of customers with product usage data

### Enable

Onboarding and enablement for this cohort is primarily through webinar cohorts, with TAM touchpoints throughout the first 60 days to ensure the customer is setup for success and has overcome initial roadblocks to adoption and value.  Enablement webinars and adhoc sessions with the TAM ensure the customer has key adoption questions answered and has the best practice guidance needed to be successful.  Use Case Health Scores enable the TAM to track the efficacy of these programs and determine strategic reach-outs.

#### Metrics for Enable

Onboarding:

1. Time to first engage: 14 days
1. Time to 1st Value: 30 days
1. Time to Onboard: 45 days
1. Net-new customer attendance in onboarding webinars
1. Onboarding NPS & CSAT scores

Use-Case Enablement

1. Customer attendance in enablement webinars
1. Use Case Health scores

### Expand & Renew

Expansion is primarily driven by the SAL or AE in this segment, though a key driver for expansion is product enablement and familiarity.  Webinars are the primary means of driving interest for customers into new segments, the success of which is tracked through customer engagement scorecards and product usage data insights around new use cases adopted.  Low-license utilization reports will focus the TAM on identifying those customers at risk of contraction. The renewal NPS/CSAT survey 110 days before renewal enables the TAM to identify customers that may be challenged at the point of renewal and are not easily identifiable as challenged through product usage data.

#### Metrics for Expand & Renew

1. Customer attendance in expansion webinars
1. New Use Cases Adopted
1. % Low License Utilizations 'Saves' (improvement)
1. Renewal NPS & CSAT Scores
