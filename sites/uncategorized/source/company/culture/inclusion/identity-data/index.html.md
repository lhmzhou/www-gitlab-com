---
layout: handbook-page-toc
title: "Identity data"
description: "GitLab country specific data in regard to team members location, gender, ethnicity, race, age etc. View data here!"
canonical_path: "/company/culture/inclusion/identity-data/"
---

#### GitLab Identity Data

Data as of 2021-07-31

##### Country Specific Data

| **Country Information**                     | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Based in APAC                               | 154       | 11.17%          |
| Based in EMEA                               | 390       | 28.28%          |
| Based in LATAM                              | 29        | 2.10%           |
| Based in NORAM                              | 806       | 58.45%          |
| **Total Team Members**                      | **1,379** | **100%**        |

##### Gender Data

| **Gender (All)**                            | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men                                         | 930       | 67.44%          |
| Women                                       | 443       | 32.12%          |
| Other Gender Identities                     | 6         | 0.44%           |
| **Total Team Members**                      | **1,379** | **100%**        |

| **Gender in Management**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Management                           | 118       | 59.60%          |
| Women in Management                         | 79        | 39.90%          |
| Other Gender Management                     | 1         | 0.51%           |
| **Total Team Members**                      | **198**   | **100%**        |

| **Gender in Leadership**                    | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Leadership                           | 80        | 68.97%          |
| Women in Leadership                         | 36        | 31.03%          |
| Other Gender Identities                     | 0         | 0%              |
| **Total Team Members**                      | **116**   | **100%**        |

| **Gender in Engineering**                   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| Men in Engineering                          | 450       | 80.07%          |
| Women in Engineering                        | 111       | 19.75%          |
| Other Gender Identities                     | 1         | 0.18%           |
| **Total Team Members**                      | **562**   | **100%**        |

##### Race/Ethnicity Data

| **Race/Ethnicity (US Only)**                | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.27%           |
| Asian                                       | 79        | 10.49%          |
| Black or African American                   | 24        | 3.19%           |
| Hispanic or Latino                          | 47        | 6.24%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.13%           |
| Two or More Races                           | 37        | 4.91%           |
| White                                       | 563       | 74.77%          |
| **Total Team Members**                      | **753**   | **100%**        |

| **Race/Ethnicity in Engineering (US Only)** | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 20        | 10.20%          |
| Black or African American                   | 3         | 1.53%           |
| Hispanic or Latino                          | 8         | 4.09%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 12        | 6.12%           |
| White                                       | 153       | 78.06%          |
| **Total Team Members**                      | **196**   | **100%**        |

| **Race/Ethnicity in Management (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 13        | 10.16%          |
| Black or African American                   | 4         | 3.13%           |
| Hispanic or Latino                          | 6         | 4.69%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 7         | 5.47%           |
| White                                       | 98        | 76.56%          |
| **Total Team Members**                      | **128**   | **100%**        |

| **Race/Ethnicity in Leadership (US Only)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 18        | 18.56%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 0         | 0.00%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 3.09%           |
| White                                       | 76        | 78.35%          |
| **Total Team Members**                      | **97**    | **100%**        |

| **Race/Ethnicity (Global)**                 | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 2         | 0.15%           |
| Asian                                       | 172       | 12.47%          |
| Black or African American                   | 41        | 2.97%           |
| Hispanic or Latino                          | 85        | 6.16%           |
| Native Hawaiian or Other Pacific Islander   | 3         | 0.22%           |
| Two or More Races                           | 50        | 3.36%           |
| White                                       | 875       | 63.45%          |
| Unreported                                  | 151       | 10.95%          |
| **Total Team Members**                      | **1,379** | **100%**        |

| **Race/Ethnicity in Engineering (Global)**  | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 68        | 12.10%          |
| Black or African American                   | 13        | 2.31%           |
| Hispanic or Latino                          | 34        | 6.05%           |
| Native Hawaiian or Other Pacific Islander   | 1         | 0.18%           |
| Two or More Races                           | 21        | 3.74%           |
| White                                       | 342       | 60.85%          |
| Unreported                                  | 83        | 14.77%          |
| **Total Team Members**                      | **562**   | **100%**        |

| **Race/Ethnicity in Management (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 21        | 10.61%          |
| Black or African American                   | 5         | 2.53%           |
| Hispanic or Latino                          | 8         | 4.04%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 8         | 4.04%           |
| White                                       | 141       | 71.21%          |
| Unreported                                  | 15        | 7.58%           |
| **Total Team Members**                      | **198**   | **100%**        |

| **Race/Ethnicity in Leadership (Global)**   | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| American Indian or Alaska Native            | 0         | 0.00%           |
| Asian                                       | 19        | 16.38%          |
| Black or African American                   | 0         | 0.00%           |
| Hispanic or Latino                          | 1         | 0.86%           |
| Native Hawaiian or Other Pacific Islander   | 0         | 0.00%           |
| Two or More Races                           | 3         | 2.59%           |
| White                                       | 85        | 73.28%          |
| Unreported                                  | 8         | 6.9%            |
| **Total Team Members**                      | **116**   | **100%**        |

##### Age Distribution

| **Age Distribution (Global)**               | **Total** | **Percentages** |
|---------------------------------------------|-----------|-----------------|
| 18-24                                       | 15        | 1.09%           |
| 25-29                                       | 214       | 15.52%          |
| 30-34                                       | 373       | 27.05%          |
| 35-39                                       | 333       | 24.15%          |
| 40-49                                       | 298       | 21.61%          |
| 50-59                                       | 120       | 8.70%           |
| 60+                                         | 26        | 1.89%           |
| **Total Team Members**                      | **1,379** | **100%**        |

**Of Note**: `Management` refers to Team Members who are *People Managers*, whereas `Leadership` denotes Team Members who are in *Director-level positions and above*.

**Source**: GitLab's HRIS, BambooHR
